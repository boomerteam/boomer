# What is this? #

Boomer. A pre-alpha Bomberman type game as an exploration into UE4.

# Features #

* 2 Player local (on same keyboard)
* Time based point game - bomb the other player more than they bomb you, wiuthout bombing yourself
* Randomly generated maps
* Powerups randomly drop from destroyed walls

# Getting Started #

* WSAD+Space and Arrows+Enter for first and second player
* Created in Unreal Editor 4.18

# More Info

* Project tracking here: https://wrinkled.myjetbrains.com/youtrack/agiles/88-0/89-0
* BitBucket here: https://bitbucket.org/boomerteam/boomer/overview
* Blog here: https://bitbucket.org/boomerteam/boomer/wiki/Blog

# 3rd Party Assets #

* Various Starter Content from UE4 Engine install for placeholder art / BluePrints
* Player Sprite: https://docs.unrealengine.com/latest/attachments/Engine/Paper2D/HowTo/TopDown/SampleAssets.rar

# About Me

I've been a software developer for about 20 years. Roughly 8 years in the game industry on both AAA PC MMO titles and IOS/Android mobile titles. I've 
moved out of the game industry the last 5 years into a serious-games type position where much of the same technology is used, but for a 
different purpose. Having had experience in many engines, I haven't looked at the latest Unreal since about 2006. This project is an attempt to dive right 
in with a goal to see what can be accomplished in a short amount of time, with a focus on C++ versus scripting (BluePrints). BluePrints are still used in 
a few key areas, such as UI, as well as for object composition. The blog above contains my notes from the process.

A little more about me: http://www.wrinkled.com