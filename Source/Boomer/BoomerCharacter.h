// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
//#include "../../../Plugins/2D/Paper2D/Source/Paper2D/Classes/PaperCharacter.h"
#include "PaperCharacter.h"
#include "BoomerCharacter.generated.h"

UCLASS(Blueprintable)
class ABoomerCharacter : public APaperCharacter
{
	GENERATED_BODY()

public:
	ABoomerCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual float TakeDamage
	(
		float DamageAmount,
		struct FDamageEvent const & DamageEvent,
		AController * EventInstigator,
		AActor * DamageCauser
	);

	virtual void BeginPlay() override;

	void AddBonusSpeed(float percent);

	UPROPERTY(EditAnywhere, Category = Fun)
	uint32 maxLives;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = Fun)
	int32 livesLeft;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = Fun)
	int32 points;

protected:
	float _bonusSpeed;
	float _oldSpeed = 0;
	float _bonusSpeedTime;
};

