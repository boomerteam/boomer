// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BoomerPlayerController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "BoomerCharacter.h"
#include "BoomerGameMode.h"
#include "BombDropper.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

ABoomerPlayerController::ABoomerPlayerController()
{
	_moveDirection = FVector::ZeroVector;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ABoomerPlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeGameAndUI());
	bShowMouseCursor = true;

	//auto pc1 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 0);
	//auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	//if (pc1->GetPawn() == GetPawn())
}

void ABoomerPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	HandleMovement();
}

void ABoomerPlayerController::HandleMovement()
{
	// keep updating the destination every tick while desired
	if (_moveDirection.X != 0 || _moveDirection.Y != 0)
	{
		APawn* const MyPawn = GetPawn();
		if (MyPawn)
		{
			UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
			auto location = MyPawn->GetActorLocation();
			location.Y += _moveDirection.Y * 180;
			// Only allow us to move up and down, not diagonal
			location.X += (_moveDirection.Y != 0) ? 0 : _moveDirection.X * 180;

			if (NavSys)
			{
				NavSys->SimpleMoveToLocation(this, location);
			}
		}
	}
}

void ABoomerPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("MoveRight", IE_Pressed, this, &ABoomerPlayerController::OnMoveRight);
	InputComponent->BindAction("MoveRight", IE_Released, this, &ABoomerPlayerController::OnMoveRightOff);
	InputComponent->BindAction("MoveLeft", IE_Pressed, this, &ABoomerPlayerController::OnMoveLeft);
	InputComponent->BindAction("MoveLeft", IE_Released, this, &ABoomerPlayerController::OnMoveLeftOff);
	InputComponent->BindAction("MoveUp", IE_Pressed, this, &ABoomerPlayerController::OnMoveUp);
	InputComponent->BindAction("MoveUp", IE_Released, this, &ABoomerPlayerController::OnMoveUpOff);
	InputComponent->BindAction("MoveDown", IE_Pressed, this, &ABoomerPlayerController::OnMoveDown);
	InputComponent->BindAction("MoveDown", IE_Released, this, &ABoomerPlayerController::OnMoveDownOff);
	InputComponent->BindAction("Action", IE_Pressed, this, &ABoomerPlayerController::DropBomb);

	InputComponent->BindAction("MoveRight2", IE_Pressed, this, &ABoomerPlayerController::OnMoveRight2);
	InputComponent->BindAction("MoveRight2", IE_Released, this, &ABoomerPlayerController::OnMoveRightOff2);
	InputComponent->BindAction("MoveLeft2", IE_Pressed, this, &ABoomerPlayerController::OnMoveLeft2);
	InputComponent->BindAction("MoveLeft2", IE_Released, this, &ABoomerPlayerController::OnMoveLeftOff2);
	InputComponent->BindAction("MoveUp2", IE_Pressed, this, &ABoomerPlayerController::OnMoveUp2);
	InputComponent->BindAction("MoveUp2", IE_Released, this, &ABoomerPlayerController::OnMoveUpOff2);
	InputComponent->BindAction("MoveDown2", IE_Pressed, this, &ABoomerPlayerController::OnMoveDown2);
	InputComponent->BindAction("MoveDown2", IE_Released, this, &ABoomerPlayerController::OnMoveDownOff2);
	InputComponent->BindAction("Action2", IE_Pressed, this, &ABoomerPlayerController::DropBomb2);

	InputComponent->BindAction("Menu", IE_Pressed, this, &ABoomerPlayerController::Menu);
}

void ABoomerPlayerController::Menu()
{
	ABoomerGameMode* gameMode = Cast<ABoomerGameMode>(GetWorld()->GetAuthGameMode());
	gameMode->ShowPauseMenu(true);
}

void ABoomerPlayerController::OnMoveRight()
{
	// set flag to keep updating destination until released
	_moveDirection.Y += 1;
}

void ABoomerPlayerController::OnMoveRightOff()
{
	// set flag to keep updating destination until released
	_moveDirection.Y -= 1;
}

void ABoomerPlayerController::OnMoveLeft()
{
	// set flag to keep updating destination until released
	_moveDirection.Y -= 1;
}

void ABoomerPlayerController::OnMoveUp()
{
	// set flag to keep updating destination until released
	_moveDirection.X += 1;
}

void ABoomerPlayerController::OnMoveDown()
{
	// set flag to keep updating destination until released
	_moveDirection.X -= 1;
}
void ABoomerPlayerController::OnMoveLeftOff()
{
	// set flag to keep updating destination until released
	_moveDirection.Y += 1;
}

void ABoomerPlayerController::OnMoveUpOff()
{
	// set flag to keep updating destination until released
	_moveDirection.X -= 1;
}

void ABoomerPlayerController::OnMoveDownOff()
{
	// set flag to keep updating destination until released
	_moveDirection.X += 1;
}

void ABoomerPlayerController::DropBomb()
{
	TArray<UBombDropper*> comps;
	GetPawn()->GetComponents<UBombDropper>(comps);
	if (comps.Num() != 0)
	{
		comps[0]->DropBomb();
	}
}

void ABoomerPlayerController::OnMoveRight2()
{
	// set flag to keep updating destination until released
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->OnMoveRight(); }
}

void ABoomerPlayerController::OnMoveRightOff2()
{
	// set flag to keep updating destination until released
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->OnMoveRightOff(); }
}
void ABoomerPlayerController::OnMoveLeft2()
{
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->OnMoveLeft(); }
}

void ABoomerPlayerController::OnMoveUp2()
{
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->OnMoveUp(); }
}

void ABoomerPlayerController::OnMoveDown2()
{
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->OnMoveDown(); }
}
void ABoomerPlayerController::OnMoveLeftOff2()
{
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->OnMoveLeftOff(); }
}

void ABoomerPlayerController::OnMoveUpOff2()
{
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->OnMoveUpOff(); }
}

void ABoomerPlayerController::OnMoveDownOff2()
{
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->OnMoveDownOff(); }
}

void ABoomerPlayerController::DropBomb2()
{
	auto pc2 = UGameplayStatics::GetPlayerController(GetPawn()->GetWorld(), 1);
	if (pc2) { Cast<ABoomerPlayerController>(pc2)->DropBomb(); }
}