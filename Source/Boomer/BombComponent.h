// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BombComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BOOMER_API UBombComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBombComponent();
	UPROPERTY(EditAnywhere, Category = Fun)
	float ExplosionRange;
	UPROPERTY(EditAnywhere, Category = Fun)
	float LifeTime;
	UPROPERTY(EditAnywhere, Category = Fun)
	TSubclassOf<class AActor> ExplosionFX;

	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	void Explode();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void PlayFX();
	float _timeElapsed;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
