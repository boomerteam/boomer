// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BoomerPlayerController.generated.h"

UCLASS()
class ABoomerPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ABoomerPlayerController();
	void BeginPlay();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	FVector _moveDirection;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	// Movement events
	void HandleMovement();
	void OnMoveRight();
	void OnMoveRightOff();
	void OnMoveLeft();
	void OnMoveLeftOff();
	void OnMoveUp();
	void OnMoveUpOff();
	void OnMoveDown();
	void OnMoveDownOff();
	void DropBomb();

	//Secondplayer
	void OnMoveRight2();
	void OnMoveRightOff2();
	void OnMoveLeft2();
	void OnMoveLeftOff2();
	void OnMoveUp2();
	void OnMoveUpOff2();
	void OnMoveDown2();
	void OnMoveDownOff2();
	void DropBomb2();

	void Menu();
};


