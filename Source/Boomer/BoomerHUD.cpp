// Fill out your copyright notice in the Description page of Project Settings.

#include "BoomerHUD.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"



ABoomerHUD::ABoomerHUD() {
	// notice that at this point we can't guarantee that the playerController is actually constructed yet, so we can't get a reference to it
	static ConstructorHelpers::FClassFinder<UUserWidget> hudWidgetObj(TEXT("/Game/HUD/HUD"));
	if (hudWidgetObj.Succeeded()) {
		_hudWidgetClass = hudWidgetObj.Class;
	}
	else {
		// hudWidgetObj not found
		_hudWidgetClass = nullptr;
	}
}

void ABoomerHUD::BeginPlay() {
	Super::BeginPlay();
	if (_hudWidgetClass)
	{
		// the player controller should be constructed by now so we can get a reference to it
		_hudWidget = CreateWidget<UUserWidget>(GetOwningPlayerController(), _hudWidgetClass);
		_hudWidget->AddToViewport();
	}
}

