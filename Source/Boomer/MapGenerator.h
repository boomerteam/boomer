// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MapGenerator.generated.h"


UCLASS()
class BOOMER_API AMapGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, Category = Fun)
	TSubclassOf<class AActor> IndestructibleObject;
	UPROPERTY(EditAnywhere, Category = Fun)
	TSubclassOf<class AActor> DestructibleObject;
	UPROPERTY(EditAnywhere, Category = Fun)
	uint32 MapWidth;
	UPROPERTY(EditAnywhere, Category = Fun)
	uint32 MapHeight;
	UPROPERTY(EditAnywhere, Category = Fun)
	float ObjectScale;
	UPROPERTY(EditAnywhere, Category = Fun)
	float PercentDestructible;
	UPROPERTY(EditAnywhere, Category = Fun)
	float PercentIndestructible;

	UPROPERTY()
	TArray<AActor*> _wallPieces;

	FVector calculatedObjectDimensions;

	// Sets default values for this actor's properties
	AMapGenerator();

	void RespawnMap();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void GenerateGrid(std::vector<uint8> & map, uint32 gridWidth, uint32 gridHeight);
	bool Solve(std::vector<uint8> & map, uint32 x, uint32 y, uint32 gridWidth, uint32 gridHeight, bool& abort);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
