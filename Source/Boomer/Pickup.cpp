// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickup.h"
#include "BoomerCharacter.h"
#include "BombDropper.h"
#include "Runtime/Engine/Public/EngineUtils.h"


// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	OnActorBeginOverlap.AddDynamic(this, &APickup::Pickup);
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickup::Pickup(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor->IsA(ABoomerCharacter::StaticClass()))
	{
		if (bombCountBonus)
		{
			TArray<UBombDropper*> Components;
			Cast<ABoomerCharacter>(OtherActor)->GetComponents<UBombDropper>(Components);
			if (Components.Num())
			{
				Components[0]->AddBonusBombs(bombCountBonus);
			}
		}
		else if (speedIncrease)
		{
			Cast<ABoomerCharacter>(OtherActor)->AddBonusSpeed(speedIncrease);
		}
	}

	Destroy();
}

