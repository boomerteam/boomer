// Fill out your copyright notice in the Description page of Project Settings.

#include "BombDropper.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "MapGenerator.h"

// Sets default values for this component's properties
UBombDropper::UBombDropper()
	: _bonusBombs(0)
	, _bonusBombTime(0)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}


// Called when the game starts
void UBombDropper::BeginPlay()
{
	Super::BeginPlay();

	//Store this for later use to quantize our placement
	TActorIterator< AMapGenerator > ActorItr(GetWorld());
	if (ActorItr)
	{
		_mapGenerator = *ActorItr;
	}
}

void UBombDropper::AddBonusBombs(uint32 bonus)
{
	_bonusBombTime = 7.0;
	_bonusBombs = bonus;
}

// Called every frame
void UBombDropper::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (_bonusBombTime > 0)
	{
		_bonusBombTime -= DeltaTime;
		if (_bonusBombTime <= 0)
		{
			_bonusBombs = 0;
			_bonusBombTime = 0;
		}
	}
}

bool UBombDropper::DropBomb()
{
	if (nullptr != Bomb)
	{
		for (int32 index = _activeBombs.Num()-1; index >= 0; --index)
		{
			if (nullptr == _activeBombs[index] || _activeBombs[index]->IsActorBeingDestroyed())
			{
				_activeBombs.RemoveAt(index, 1, false);
			}
		}
		if (_activeBombs.Num() < (int32)MaxActiveBombs + (int32)_bonusBombs)
		{
			auto curLocation = GetOwner()->GetActorLocation();

			// Could cache all of this
			auto generatorLocation = _mapGenerator->GetActorLocation();
			FVector2D size(_mapGenerator->MapWidth, _mapGenerator->MapHeight);
			int32 gridHeight = size.Y / (_mapGenerator->calculatedObjectDimensions.Y / 2);
			int32 gridWidth = size.X / (_mapGenerator->calculatedObjectDimensions.X / 2);
			//FVector2D maxSize((float)gridWidth * _mapGenerator->calculatedObjectDimensions.Y / 2, (float)gridHeight * _mapGenerator->calculatedObjectDimensions.X / 2);

			int32 gridX = (int)(round((curLocation.X - generatorLocation.X) / (_mapGenerator->calculatedObjectDimensions.X)));
			if (gridX <= -gridHeight) { gridX = -gridHeight + 1; }; // X is in negative space
			auto newX = generatorLocation.X + (_mapGenerator->calculatedObjectDimensions.X) * gridX;
			int32 gridY = (int)(round((curLocation.Y - generatorLocation.Y) / (_mapGenerator->calculatedObjectDimensions.Y)));
			if (gridY >= gridWidth) { gridX = gridWidth - 1; };
			auto newY = generatorLocation.Y + (_mapGenerator->calculatedObjectDimensions.Y) * gridY;

			auto spawn = GetWorld()->SpawnActor<AActor>(Bomb, FVector(newX, newY, curLocation.Z), FRotator::ZeroRotator);
			_activeBombs.Push(spawn);
			return true;
		}
	}

	return false;
}

