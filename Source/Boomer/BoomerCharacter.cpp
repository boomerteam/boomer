// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BoomerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "BoomerPlayerController.h"
#include "PaperFlipbookComponent.h"
#include "GameFramework/PlayerStart.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

ABoomerCharacter::ABoomerCharacter()
	: _bonusSpeedTime (0)
	, _oldSpeed(0)
{
	livesLeft = maxLives;
	points = 0;

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ABoomerCharacter::AddBonusSpeed(float percent)
{
	_bonusSpeedTime = 5.0;

	if (_oldSpeed != 0)
	{
		GetCharacterMovement()->MaxWalkSpeed = _oldSpeed;
	}

	_oldSpeed = GetCharacterMovement()->MaxWalkSpeed;
	GetCharacterMovement()->MaxWalkSpeed = GetCharacterMovement()->MaxWalkSpeed*percent;
}

void ABoomerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (_bonusSpeedTime > 0)
	{
		_bonusSpeedTime -= DeltaSeconds;
		if (_bonusSpeedTime <= 0)
		{
			GetCharacterMovement()->MaxWalkSpeed = _oldSpeed;
			_oldSpeed = 0;
			_bonusSpeedTime = 0;
		}
	}
}

void ABoomerCharacter::BeginPlay()
{
	Super::BeginPlay();

	bool isPlayer2 = GEngine->GetNumGamePlayers(GetWorld()) > 1;// GetGamePlayer(GetWorld(), 0)->GetPlayerController(GetWorld()) != GetController();
		//Cast<ABoomerPlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()))->GetPawnOrSpectator()->GetUniqueID() != GetUniqueID();

	if (isPlayer2)
	{
		// Change my color???
		TArray<UPaperFlipbookComponent*> Components;
		GetComponents<UPaperFlipbookComponent>(Components);
		if (Components.Num())
		{
			FLinearColor newColor(1,0,0);
			Components[0]->SetSpriteColor(newColor);
		}
	}
}

float ABoomerCharacter::TakeDamage
(
	float DamageAmount,
	struct FDamageEvent const & DamageEvent,
	AController * EventInstigator,
	AActor * DamageCauser
)
{
	bool isPlayer2 = Cast<ABoomerPlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld())) != GetController();
	if (isPlayer2)
	{
		Cast<ABoomerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))->points++;
	}
	else
	{
		Cast<ABoomerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 1))->points++;
	}
	AActor* start = nullptr;
	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		APlayerStart* PlayerStart = *It;

		if (isPlayer2 && PlayerStart->PlayerStartTag == "Player2")
		{
			start =  PlayerStart;
		}
		else if (!isPlayer2 && PlayerStart->PlayerStartTag == "Player1")
		{
			start = PlayerStart;
		}
	}
	this->SetActorLocation(start->GetActorLocation());
	Cast<ABoomerPlayerController>(GetController())->StopMovement();
	return DamageAmount;
}
