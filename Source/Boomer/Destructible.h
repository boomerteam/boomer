// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Destructible.generated.h"

class APickup;

UCLASS()
class BOOMER_API ADestructible : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, Category = Fun)
	TArray<TSubclassOf<class APickup> > Pickups;

	UPROPERTY(EditAnywhere, Category = Fun)
		float pickupChance;

	// Sets default values for this actor's properties
	ADestructible();
	float TakeDamage
	(
		float DamageAmount,
		struct FDamageEvent const & DamageEvent,
		AController * EventInstigator,
		AActor * DamageCauser
	);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
