// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BoomerGameMode.generated.h"

UCLASS(minimalapi)
class ABoomerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	/** Remove the current menu widget and create a new one from the specified class, if provided. */
	UFUNCTION(BlueprintCallable, Category = "Fun")
	void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

	UFUNCTION(BlueprintCallable, Category = "Fun")
	void SetGamePaused(bool bIsPaused);

	UFUNCTION(BlueprintCallable, Category = "Fun")
	void ShowPauseMenu(bool bShow);

	UFUNCTION(BlueprintCallable, Category = "Fun")
	void Reset();

	AActor* ChoosePlayerStart_Implementation(AController* Player);

	/** The widget class we will use as our menu when the game starts. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Fun")
	TSubclassOf<UUserWidget> StartingWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Fun")
	float gameLength;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Fun")
	float curGameTime;

	/** The widget instance that we are using as our menu. */
	UPROPERTY()
	UUserWidget* CurrentWidget;

	TSubclassOf<UUserWidget> PauseWidgetClass;
public:
	ABoomerGameMode();

	void BeginPlay();

	void Tick(float deltaSeconds) override;
};



