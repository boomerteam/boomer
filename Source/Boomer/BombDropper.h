// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


class AMapGenerator;

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BombDropper.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BOOMER_API UBombDropper : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBombDropper();

	UPROPERTY(EditAnywhere, Category = Fun)
	TSubclassOf<class AActor> Bomb;
	UPROPERTY(EditAnywhere, Category = Fun)
	uint32 MaxActiveBombs;

	bool DropBomb();
	void AddBonusBombs(uint32 bonus);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY()
	TArray<AActor*> _activeBombs;

	AMapGenerator * _mapGenerator;

	uint32 _bonusBombs;
	float _bonusBombTime;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
