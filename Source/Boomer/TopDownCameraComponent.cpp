// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownCameraComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UTopDownCameraComponent::UTopDownCameraComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTopDownCameraComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTopDownCameraComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != this->GetOwner())
		{
			// Cut instantly to camera one.
			OurPlayerController->SetViewTarget(this->GetOwner());
		}
	}
}

