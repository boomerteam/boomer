// Fill out your copyright notice in the Description page of Project Settings.

#include "MapGenerator.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include <vector>

// Sets default values
AMapGenerator::AMapGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	ObjectScale = 1.0f;
}

// Called when the game starts or when spawned
void AMapGenerator::BeginPlay()
{
	Super::BeginPlay();
	RespawnMap();
}

void AMapGenerator::RespawnMap()
{
	for (auto& piece : _wallPieces)
	{
		if (nullptr != piece && !piece->IsActorBeingDestroyed())
		{
			piece->Destroy();
		}
	}

	_wallPieces.Empty();

	if (nullptr != IndestructibleObject && GetWorld())
	{
		auto start = GetActorLocation();
		auto spawn = GetWorld()->SpawnActor<AActor>(IndestructibleObject, start, FRotator::ZeroRotator);
		FVector Origin;
		spawn->GetActorBounds(false, Origin, calculatedObjectDimensions);
		calculatedObjectDimensions *= ObjectScale;
		spawn->Destroy();
		uint32 gridWidth = MapWidth / (calculatedObjectDimensions.Y  /2);
		uint32 gridHeight = MapHeight / (calculatedObjectDimensions.X /2);

		std::vector<uint8> map;
		GenerateGrid(map, gridWidth, gridHeight);

		//Now Spawn
		for (uint32 height = 0; height < gridHeight; ++height)
		{
			for (uint32 width = 0; width < gridWidth; ++width)
			{
				// Skip the 2 corners for the 2 players - prob should do this in grid generation
				if ((0 == height && 0 == width) || (height == gridHeight-1 && width == gridWidth-1)) { continue; }
				auto spawnSpot = map[height*gridWidth + width];
				if (spawnSpot)
				{
					spawn = GetWorld()->SpawnActor<AActor>(1 == spawnSpot?IndestructibleObject:DestructibleObject, start + FVector4(height * -calculatedObjectDimensions.X , width * calculatedObjectDimensions.Y , 0), FRotator::ZeroRotator);
				}
				if (nullptr != spawn)
				{
					_wallPieces.Push(spawn);
					FTransform CurTrans = spawn->GetTransform();
					CurTrans.SetScale3D(FVector(ObjectScale, ObjectScale, ObjectScale));
					spawn->SetActorTransform(CurTrans, false, nullptr, ETeleportType::TeleportPhysics);
				}
			}
		}
	}
}

// Based off https://stackoverflow.com/questions/9191428/maze-solving-algorithm-in-c
bool AMapGenerator::Solve(std::vector<uint8>& map, uint32 x, uint32 y, uint32 gridWidth, uint32 gridHeight, bool& abort)
{
	// Check if we have reached our goal.
	if (x == gridWidth - 1 && y == gridHeight - 1)
	{
		return true;
	}

	// This is more of an assert when I was debugging an issue 
	if (x >= gridWidth || y >= gridHeight)
	{
		return false;
	}

	if (!abort)
	{
		// Make the move (if it's wrong, we will backtrack later.
		const uint8 cPlayer = 254;
		map[y*gridWidth + x] = cPlayer;

		// Recursively search for our goal.
		if (x > 0 && map[y * gridWidth + x - 1] == 0 && Solve(map, x - 1, y, gridWidth, gridHeight, abort))
		{
			return true;
		}
		if (x < gridWidth-1 && map[y * gridWidth + x + 1] == 0 && Solve(map, x + 1, y, gridWidth, gridHeight, abort))
		{
			return true;
		}
		if (y > 0 && map[(y - 1) * gridWidth + x] == 0 && Solve(map, x, y - 1, gridWidth, gridHeight, abort))
		{
			return true;
		}
		if (y < gridHeight-1 && map[(y + 1) * gridWidth + x + 1] == 0 && Solve(map, x, y + 1, gridWidth, gridHeight, abort))
		{
			return true;
		}
		else
		{
			// MASSIVE FAIL - get out entirely now
			abort = true;
		}
	}

	// Otherwise we need to backtrack and find another solution.
	map[y*gridWidth + x] = 0;
	return false;
}

void AMapGenerator::GenerateGrid(std::vector<uint8> & map, uint32 gridWidth, uint32 gridHeight)
{
	bool solved = false;
	map.resize(gridWidth*gridHeight);

	while (!solved)
	{
		map[0] = 0; // Player start

		for (auto i = 1; i < map.size(); ++i)
		{
			auto val = rand() % 100;
			if (val < PercentIndestructible * 100)
			{
				map[i] = 1;
			}
			else
			{
				map[i] = 0;
			}
		}

		bool abort = false;
		solved = Solve(map, 0, 0, gridWidth, gridHeight, abort);
	}

	// Now fill with destructible

	for (uint32 i = 0; i < map.size(); ++i)
	{
		// Clear out our player junk now from the solve
		if (254 == map[i]) {
			map[i] = 0;
		};
		// Create a safe space
		if (0 == map[i] && i > 3 && i != gridWidth && i != gridWidth*2 && i != gridWidth * 3
			&& i < gridWidth * gridHeight - 4 && i != gridWidth * gridHeight -1 && i != gridWidth * (gridHeight - 1) - 1 && i != gridWidth * (gridHeight - 2) - 1)
		{
			auto val = rand() % 100;
			if (val < PercentDestructible * 100)
			{
				map[i] = 2;
			}
		}
	}
}

// Called every frame
void AMapGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

