// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BoomerGameMode.h"
#include "BoomerPlayerController.h"
#include "BoomerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "BoomerHUD.h"
#include "Blueprint/UserWidget.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "MapGenerator.h"

ABoomerGameMode::ABoomerGameMode()
{
	curGameTime = 0;

	// use our custom PlayerController class
	PlayerControllerClass = ABoomerPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/BoomerCharacter_New"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	static ConstructorHelpers::FClassFinder<UUserWidget> MainMenu(TEXT("/Game/HUD/MainMenu"));
	if (MainMenu.Class != NULL)
	{
		StartingWidgetClass = MainMenu.Class;
	}

	static ConstructorHelpers::FClassFinder<UUserWidget> Menu(TEXT("/Game/HUD/PauseMenu"));
	if (Menu.Class != NULL)
	{
		PauseWidgetClass = Menu.Class;
	}

	HUDClass = ABoomerHUD::StaticClass();

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	gameLength = 120;
}

void ABoomerGameMode::BeginPlay()
{
	Super::BeginPlay();
	ChangeMenuWidget(StartingWidgetClass);
	SetGamePaused(true);
}

void ABoomerGameMode::Tick(float deltaSeconds)
{
	Super::Tick(deltaSeconds);
	curGameTime += deltaSeconds;

	if (curGameTime > gameLength)
	{
		//@TODO Show win screen
		if (PauseWidgetClass != NULL)
		{
			ChangeMenuWidget(PauseWidgetClass);
		}
		curGameTime = gameLength;
		SetGamePaused(true);
	}
}

void ABoomerGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	SetGamePaused(false);
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}
	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}


void ABoomerGameMode::SetGamePaused(bool bIsPaused)
{
	ABoomerPlayerController* const MyPlayer = Cast<ABoomerPlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
	if (MyPlayer != NULL)
	{
		MyPlayer->SetPause(bIsPaused);
		MyPlayer->bShowMouseCursor = bIsPaused;
	}
}

void ABoomerGameMode::ShowPauseMenu(bool show)
{
	if (show)
	{
		if (PauseWidgetClass != NULL)
		{
			ChangeMenuWidget(PauseWidgetClass);
		}
	}
	else
	{
		ChangeMenuWidget(nullptr);
	}
	SetGamePaused(show);
}

void ABoomerGameMode::Reset()
{

	TActorIterator< AMapGenerator > ActorItr(GetWorld());
	if (ActorItr)
	{
		ActorItr->RespawnMap();
	}

	auto player = GEngine->GetGamePlayer(GetWorld(), 0);
	if (player)
	{
		auto pc = player->GetPlayerController(GetWorld());
		if (pc)
		{
			UGameplayStatics::RemovePlayer(pc, true);
		}
	}
	auto players = GEngine->GetNumGamePlayers(GetWorld());
	if (players > 0)
	{
		player = GEngine->GetGamePlayer(GetWorld(), 0);
		if (player)
		{
			auto pc = player->GetPlayerController(GetWorld());
			if (pc)
			{
				UGameplayStatics::RemovePlayer(pc, true);
			}
		}
	}
	//this->RestartPlayer(GEngine->GetGamePlayer(GetWorld(), 0)->GetPlayerController(GetWorld()));
	//this->RestartPlayer(GEngine->GetGamePlayer(GetWorld(), 1)->GetPlayerController(GetWorld()));

	//Create our second player
	UGameplayStatics::CreatePlayer(GetWorld(), 0, true);
	UGameplayStatics::CreatePlayer(GetWorld(), 1, true);

	curGameTime = 0;
}

AActor* ABoomerGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	bool isPlayer2 = Cast<ABoomerPlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld())) != Player;
	AActor* start = nullptr;
	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		APlayerStart* PlayerStart = *It;

		if (isPlayer2 && PlayerStart->PlayerStartTag == "Player2")
		{
			return PlayerStart;
		}
		else if (!isPlayer2 && PlayerStart->PlayerStartTag == "Player1")
		{
			return PlayerStart;
		}
	}

	return start;
}