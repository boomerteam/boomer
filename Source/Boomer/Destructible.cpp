// Fill out your copyright notice in the Description page of Project Settings.

#include "Destructible.h"
#include "BombComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Pickup.h"


// Sets default values
ADestructible::ADestructible()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ADestructible::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADestructible::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float ADestructible::TakeDamage
(
	float DamageAmount,
	struct FDamageEvent const & DamageEvent,
	AController * EventInstigator,
	AActor * DamageCauser
)
{
	if (DamageCauser && DamageCauser->GetUniqueID() != this->GetUniqueID())
	{
		TArray<UBombComponent*> Components;
		GetComponents<UBombComponent>(Components);
		if (Components.Num())
		{
			Components[0]->Explode();
		}
	}

	if (Pickups.Num())
	{
		if (rand() % 100 < pickupChance * 100)
		{
			int32 index = rand() % Pickups.Num();
			auto spawn = GetWorld()->SpawnActor<AActor>(Pickups[index], GetActorLocation(), FRotator::ZeroRotator);
		}
	}
	this->Destroy();

	return DamageAmount;
}

