// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "BoomerHUD.generated.h"

/**
 * 
 */
UCLASS()
class BOOMER_API ABoomerHUD : public AHUD
{
	GENERATED_BODY()

	ABoomerHUD();
	class UClass * _hudWidgetClass;
	class UUserWidget * _hudWidget;
	
	virtual void BeginPlay() override;
};
