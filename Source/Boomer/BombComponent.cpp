// Fill out your copyright notice in the Description page of Project Settings.

#include "BombComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "MapGenerator.h"

#include <set>

// Sets default values for this component's properties
UBombComponent::UBombComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
	_timeElapsed = 0;
}


// Called when the game starts
void UBombComponent::BeginPlay()
{
	Super::BeginPlay();

	TArray<UStaticMeshComponent*> Components;
	GetOwner()->GetComponents<UStaticMeshComponent>(Components);
	if (Components.Num())
	{
		Components[0]->OnComponentEndOverlap.AddDynamic(this, &UBombComponent::OnOverlapEnd);
	}
}


// Called every frame
void UBombComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	_timeElapsed += DeltaTime;
	if (_timeElapsed > LifeTime)
	{
		Explode();
	}
}

void UBombComponent::Explode()
{
	TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
	FDamageEvent DamageEvent(ValidDamageTypeClass);

	// This would have been so much easier. I'm missing something though on overlaps.
	//TSet<AActor*> actors;
	//GetOwner()->GetOverlappingActors(actors);
	//for (auto& collide : actors)
	//{
	//	collide->TakeDamage(100, DamageEvent, nullptr, GetOwner());
	//}

	TArray<FOverlapResult> overlaps;

	//First direction
	auto shape = FCollisionShape::MakeBox(FVector(ExplosionRange, 100, 1));
	std::set<uint32> alreadyDone;
	GetWorld()->OverlapMultiByObjectType(overlaps, GetOwner()->GetActorLocation(), FQuat::Identity, FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllObjects), shape, FCollisionQueryParams(false));
	for (auto& collide : overlaps)
	{
		//@TODO set owner to the damage causer?
		//@TODO Arbitrary damage
		if (nullptr == collide.Actor) { continue;  }
		alreadyDone.insert(collide.Actor->GetUniqueID());

		if (collide.Actor->bCanBeDamaged)
		{
			FHitResult castResult;
			GetWorld()->LineTraceSingleByObjectType(castResult, GetOwner()->GetActorLocation(), collide.Actor->GetActorLocation(), FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllStaticObjects));
			if (!castResult.bBlockingHit || castResult.Actor == collide.Actor || castResult.Actor == GetOwner()) // I need to make a custom collision group for JUST industructibles
			{
				collide.Actor->TakeDamage(100, DamageEvent, nullptr, GetOwner());
			}
		}
	}

	//Other direction
	shape = FCollisionShape::MakeBox(FVector(100, ExplosionRange, 1));
	GetWorld()->OverlapMultiByObjectType(overlaps, GetOwner()->GetActorLocation(), FQuat::Identity, FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllObjects), shape, FCollisionQueryParams(false));

	for (auto& collide : overlaps)
	{
		if (nullptr == collide.Actor) { continue; }
		// Make sure we haven't already tried to damage someone
		if (!collide.Actor->IsActorBeingDestroyed() && collide.Actor->bCanBeDamaged && alreadyDone.find(collide.Actor->GetUniqueID()) == alreadyDone.end())
		{
			//@TODO set owner to the damage causer?
			//@TODO Arbitrary damage
			FHitResult castResult;
			GetWorld()->LineTraceSingleByObjectType(castResult, GetOwner()->GetActorLocation(), collide.Actor->GetActorLocation(), FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllStaticObjects));
			if (!castResult.bBlockingHit || castResult.Actor == collide.Actor || castResult.Actor == GetOwner())
			{
				collide.Actor->TakeDamage(100, DamageEvent, nullptr, GetOwner());
			}
		}
	}

	//Spawn Explosions
	PlayFX();

	GetOwner()->Destroy();
}

void UBombComponent::PlayFX()
{
	// There's a LOT of room for optimization here, but brute force wins on time for now.
	// Including move this grid "snapping" function somewhere as a utility
	if (nullptr != ExplosionFX)
	{
		TActorIterator< AMapGenerator > ActorItr(GetWorld());
		AMapGenerator * mapGenerator = nullptr;
		if (ActorItr)
		{
			mapGenerator = *ActorItr;

			auto generatorLocation = mapGenerator->GetActorLocation();
			FVector2D size(mapGenerator->MapWidth, mapGenerator->MapHeight);
			int32 gridHeight = size.Y / (mapGenerator->calculatedObjectDimensions.Y / 2);
			int32 gridWidth = size.X / (mapGenerator->calculatedObjectDimensions.X / 2);

			auto curLocation = GetOwner()->GetActorLocation();
			int32 gridX = (int)(round((curLocation.X - generatorLocation.X) / (mapGenerator->calculatedObjectDimensions.X)));
			if (gridX <= -gridHeight) { gridX = -gridHeight + 1; }; // X is in negative space
			auto newX = generatorLocation.X + (mapGenerator->calculatedObjectDimensions.X) * gridX;
			int32 gridY = (int)(round((curLocation.Y - generatorLocation.Y) / (mapGenerator->calculatedObjectDimensions.Y)));
			if (gridY >= gridWidth) { gridX = gridWidth - 1; };
			auto newY = generatorLocation.Y + (mapGenerator->calculatedObjectDimensions.Y) * gridY;

			FVector center(newX, newY, curLocation.Z);
			// DO X first
			newX = center.X - ExplosionRange;
			while (newX < center.X + ExplosionRange)
			{
				FVector explosionCenter(newX, newY, curLocation.Z);
				FHitResult castResult;
				GetWorld()->LineTraceSingleByObjectType(castResult, center, explosionCenter, FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllStaticObjects));
				if (!castResult.bBlockingHit)
				{
					auto spawn = GetWorld()->SpawnActor<AActor>(ExplosionFX, explosionCenter, FRotator::ZeroRotator);
				}
				newX += mapGenerator->calculatedObjectDimensions.X;
			}

			//Now Y
			newX = center.X;
			newY = center.Y - ExplosionRange;
			while (newY < center.Y + ExplosionRange)
			{
				FVector explosionCenter(newX, newY, curLocation.Z);
				FHitResult castResult;
				GetWorld()->LineTraceSingleByObjectType(castResult, center, explosionCenter, FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllStaticObjects));
				if (!castResult.bBlockingHit)
				{
					auto spawn = GetWorld()->SpawnActor<AActor>(ExplosionFX, explosionCenter, FRotator::ZeroRotator);
				}
				newY += mapGenerator->calculatedObjectDimensions.Y;
			}
		}
	}
}

void UBombComponent::OnOverlapEnd(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	TArray<UStaticMeshComponent*> Components;
	GetOwner()->GetComponents<UStaticMeshComponent>(Components);
	if (Components.Num())
	{
		Components[0]->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	}
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, "Enable collision");
}

